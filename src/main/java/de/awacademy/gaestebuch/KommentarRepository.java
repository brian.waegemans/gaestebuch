package de.awacademy.gaestebuch;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KommentarRepository extends JpaRepository<Kommentar, Integer> {
}
