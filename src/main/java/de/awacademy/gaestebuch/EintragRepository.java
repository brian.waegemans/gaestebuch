package de.awacademy.gaestebuch;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EintragRepository extends JpaRepository<Eintrag, Integer>{
    List<Eintrag> findAllByOrderByDateDesc();
    List<Eintrag> findAllByOrderByDateAsc();
    List<Eintrag> findAllByOrderByTitelAsc();
}
