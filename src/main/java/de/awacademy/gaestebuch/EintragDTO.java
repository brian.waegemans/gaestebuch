package de.awacademy.gaestebuch;

public class EintragDTO {
    private String titel;
    private String beschreibung;

    public EintragDTO(){
        this.titel = "";
        this.beschreibung = "";
    }

    public String getTitel() {
        return titel;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }
}
