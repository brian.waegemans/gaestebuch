package de.awacademy.gaestebuch;

public class KommentarDTO {
    private String kommentarText;

    public String getKommentarText() {
        return kommentarText;
    }

    public void setKommentarText(String kommentarText) {
        this.kommentarText = kommentarText;
    }
}
