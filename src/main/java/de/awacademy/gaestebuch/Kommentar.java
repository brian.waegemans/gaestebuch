package de.awacademy.gaestebuch;

import jakarta.persistence.*;

@Entity
public class Kommentar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String kommentarText;

    @ManyToOne
    Eintrag eintrag;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKommentarText() {
        return kommentarText;
    }

    public void setKommentarText(String kommentar) {
        this.kommentarText = kommentar;
    }

    public Eintrag getEintrag() {
        return eintrag;
    }

    public void setEintrag(Eintrag eintrag) {
        this.eintrag = eintrag;
    }
}
