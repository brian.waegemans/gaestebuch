package de.awacademy.gaestebuch;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class GaestebuchController {

    EintragRepository gaestebuch;
    KommentarRepository kommentarRepository;
    public GaestebuchController(EintragRepository gaestebuch, KommentarRepository kommentarRepository){
        this.gaestebuch = gaestebuch;
        this.kommentarRepository = kommentarRepository;
    }



    @GetMapping("/")
    public String index(Model model){
        List<Eintrag> gaestebuch = this.gaestebuch.findAllByOrderByTitelAsc();
        model.addAttribute("gaestebuch", gaestebuch);
        return "index";
    }

    @GetMapping("/create-eintrag")
    public String createEintrag(Model model){
        EintragDTO eintragDTO = new EintragDTO();
        model.addAttribute("eintrag", eintragDTO);
        return "create-eintrag";
    }

    @PostMapping("/submit-eintrag")
    public String submitEintrag(@ModelAttribute EintragDTO eintragDTO){
        Eintrag eintrag = new Eintrag();
        eintrag.setTitel(eintragDTO.getTitel());
        eintrag.setBeschreibung(eintragDTO.getBeschreibung());
        gaestebuch.save(eintrag);
        return "redirect:/";
    }

    @GetMapping("/create-kommentar")
    public String createKommentar(Model model, @RequestParam int eintragId){
        KommentarDTO kommentarDTO = new KommentarDTO();
        model.addAttribute("kommentar", kommentarDTO);
        model.addAttribute("eintragId", eintragId);
        return "create-kommentar";
    }

    @PostMapping("/submit-kommentar")
    public String submitKommentar(@ModelAttribute KommentarDTO kommentarDTO, @RequestParam int eintragId){
        Integer eintragIdWrapped = eintragId;
        Kommentar kommentar = new Kommentar();
        kommentar.setKommentarText(kommentarDTO.getKommentarText());
        gaestebuch.findById(eintragIdWrapped).ifPresent(kommentar::setEintrag);
        kommentarRepository.save(kommentar);
        return "redirect:/";
    }

    @GetMapping("/newestFirst")
    public String newestFirst(Model model){
        List<Eintrag> gaestebuch = this.gaestebuch.findAllByOrderByDateDesc();
        model.addAttribute("gaestebuch", gaestebuch);
        return "index";
    }
    @GetMapping("/oldestFirst")
    public String oldestFirst(Model model){
        List<Eintrag> gaestebuch = this.gaestebuch.findAllByOrderByDateAsc();
        model.addAttribute("gaestebuch", gaestebuch);
        return "index";
    }
}
